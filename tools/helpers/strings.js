// TODO: This should move to utils repo

const capitalize = (string) => (string && string[0].toUpperCase() + string.slice(1)) || ''

const toPascalCase = (string) =>
  (string &&
    (' ' + string)
      .toLowerCase()
      .replace(/[^a-zA-Z0-9]+(.)/g, (m, chr) => chr.toUpperCase())) ||
  ''

module.exports = {
  capitalize,
  toPascalCase,
}
