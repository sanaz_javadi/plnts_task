/* eslint-disable no-console */
const fs = require('fs')

const fileExists = (path) => (file) => fs.existsSync(`${path}/${file}`)

const writeToPath = (path) => (file, content) => {
  const filePath = `${path}/${file}`

  fs.writeFile(filePath, content, (err) => {
    if (err) throw err
    console.log('Created file: ', filePath)
    return true
  })
}

module.exports = {
  fileExists,
  writeToPath,
}
