module.exports = (name) => `// TODO: write rest of ${name} tests
import { render, screen } from '@testing-library/react'

import { ${name} } from './${name}'

describe('${name}', () => {
  it('renders the ${name}', () => {
    const title = 'title'
    render(<${name} title='title' />)

    const element = screen.getByText(title)

    expect(element).toBeInTheDocument()
  })
})
`
