const componentExports = require('./exports')
const component = require('./component')
const models = require('./models')
const tests = require('./tests')
const styles = require('./styles')

module.exports = {
  index: componentExports,
  component,
  models,
  tests,
  styles,
}
