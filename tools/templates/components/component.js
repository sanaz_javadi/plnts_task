module.exports = (name) => `// TODO: write rest of ${name} component
import { I${name}Props } from './${name}.model'

export const ${name} = ({ title = 'TODO ${name}' }: I${name}Props) => {
  return <h3>{title}</h3>
}
`
