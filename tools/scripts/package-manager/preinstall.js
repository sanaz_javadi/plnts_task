/* eslint-disable no-console */
const childProcess = require('child_process')

const npmConfigArgv = JSON.parse(process.env.npm_config_argv)
const isAddCommand = npmConfigArgv.cooked[0] === 'add'

/**
 * Do NOT allow using `npm` as package manager.
 */
if (process.env.npm_execpath.indexOf('yarn') === -1) {
  console.error('You must use Yarn to install dependencies:')
  console.error('  $ yarn install')
  process.exit(1)
} /* check if yarn add is ran */ else if (isAddCommand) {
  try {
    const installingPackages = npmConfigArgv.original.filter((pkg, index) => {
      // detect if it's really a package
      if (index === 0) return false
      else if (pkg.startsWith('-')) return false
      return true
    })

    // run bundle phobia
    childProcess.execSync(
      `${process.env.npm_execpath} bundle-phobia ${installingPackages.join(' ')}`,
      {
        stdio: [0, 1, 2],
      }
    )
  } catch (error) {
    console.error('[catch bundle-phobia error] ', error.message)
  }
}
