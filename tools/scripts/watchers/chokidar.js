const chokidar = require('chokidar')
const { createFiles } = require('../scaffolders/chokidar')

const ChokidarWatcher = {
  watch: () => {
    chokidar
      .watch('src/components/**', { ignored: /node_modules/ })
      .on('addDir', (path) => {
        const name = path.replace(/.*\/components\/((.*\/)*)?/, '')
        const goodToGo = /^[^/_]*$/.test(name)
        if (goodToGo) createFiles(path, name)
      })
  },
}

module.exports = ChokidarWatcher
