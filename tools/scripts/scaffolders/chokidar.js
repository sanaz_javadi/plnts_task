/* eslint-disable no-console */

const { writeToPath, fileExists } = require('../../helpers/files')
const { toPascalCase } = require('../../helpers/strings')
const templates = require('../../templates/components')

function createFiles(path, name) {
  const pascalCasedName = toPascalCase(name)
  const files = {
    index: 'index.ts',
    component: `${pascalCasedName}.tsx`,
    models: `${pascalCasedName}.model.ts`,
    tests: `${pascalCasedName}.test.tsx`,
    styles: `${pascalCasedName}.module.scss`,
  }

  if (name !== 'components') {
    const writeFile = writeToPath(path)
    const toFileMissingBool = (file) => !fileExists(path)(file)
    const checkAllMissing = (acc, cur) => acc && cur

    const noneExist = Object.values(files).map(toFileMissingBool).reduce(checkAllMissing)

    if (noneExist) {
      console.log(`Detected new component: ${name}, ${path}`)
      Object.entries(files).forEach(([type, fileName]) => {
        writeFile(fileName, templates[type](pascalCasedName))
      })
    }
  }
}

module.exports = {
  createFiles,
}
