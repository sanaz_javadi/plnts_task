module.exports = {
  root: true,
  env: { 'cypress/globals': true },
  plugins: ['eslint-plugin-cypress'],
  extends: ['plugin:cypress/recommended'],
}
