describe('render the main page', () => {
  it('have the correct title', () => {
    cy.visit('/')
    cy.get('header h2').first().should('have.text', 'snappshop')
  })
})
