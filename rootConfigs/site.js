const SITE_DOMAIN = process.env.NEXT_PUBLIC_DOMAIN || ''
const SITE_URL = process.env.NEXT_PUBLIC_URL || ''
const THIRD_PARTY_DOMAINS = process.env.THIRD_PARTY_DOMAINS || ''

module.exports = {
  SITE_DOMAIN,
  SITE_URL,
  THIRD_PARTY_DOMAINS,
}
