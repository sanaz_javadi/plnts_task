const { SITE_DOMAIN, SITE_URL, THIRD_PARTY_DOMAINS } = require('../site')

const Permissions = [
  // https://www.w3.org/TR/permissions-policy-1/
  `fullscreen=(self "${SITE_URL}")`,
  'geolocation=()',
  'web-share=()',
]

// https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP
const ContentSecurityPolicy = `
    default-src 'self' ${SITE_DOMAIN} *.${SITE_DOMAIN} 'unsafe-inline';
    script-src 'self' ${SITE_DOMAIN} *.${SITE_DOMAIN} ${THIRD_PARTY_DOMAINS} 'unsafe-inline' 'unsafe-eval';
    child-src 'none';
    style-src 'self' ${SITE_DOMAIN} *.${SITE_DOMAIN} 'unsafe-inline';
    font-src 'self' ${SITE_DOMAIN} *.${SITE_DOMAIN} 'unsafe-inline';  
`

const securityHeaders = [
  // https://nextjs.org/docs/advanced-features/security-headers
  {
    key: 'X-DNS-Prefetch-Control',
    value: 'on',
  },
  {
    key: 'Strict-Transport-Security',
    value: `max-age=${60 * 60 * 24 * 365 * 2}; includeSubDomains; preload`,
  },
  {
    key: 'X-XSS-Protection',
    value: '1; mode=block',
  },
  {
    key: 'X-Frame-Options',
    value: 'SAMEORIGIN',
  },
  {
    key: 'Permissions-Policy',
    value: Permissions.join(', '),
  },
  {
    key: 'X-Content-Type-Options',
    value: 'nosniff',
  },
  {
    key: 'Referrer-Policy',
    value: 'strict-origin-when-cross-origin',
  },
  {
    key: 'Content-Security-Policy',
    value: ContentSecurityPolicy.replace(/\s{2,}/g, ' ').trim(),
  },
]

module.exports = {
  securityHeaders,
}
