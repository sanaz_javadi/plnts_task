export * from './{{pascalCase name}}'
export { default as {{pascalCase name}} } from './{{pascalCase name}}'
export * from './{{pascalCase name}}.model'
