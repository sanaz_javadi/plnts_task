import { render, screen } from '@testing-library/react'

import { {{pascalCase name}} } from './{{pascalCase name}}'

describe('{{pascalCase name}}', () => {
  it('renders the {{camelCase name}}', () => {
    render(<{{pascalCase name}} />)
  })
})
