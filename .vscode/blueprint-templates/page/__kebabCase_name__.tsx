import type { NextPage } from 'next'

import styles from 'styles/pages/{{pascalCase name}}.module.css'

const {{pascalCase name}}: NextPage = () => {
  return null
}

export default {{pascalCase name}}
