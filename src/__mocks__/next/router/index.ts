export const useRouter = () => ({
  route: '',
  pathname: '',
  query: {},
  asPath: '',
  push: async () => true,
  replace: async () => true,
  reload: () => null,
  back: () => null,
  prefetch: async () => undefined,
  beforePopState: () => null,
  isFallback: false,
  events: {
    on: () => null,
    off: () => null,
    emit: () => null,
  },
})
