import { GTMScript } from 'components/analytics/gtm-script'
import { getRestrictionConfig } from 'components/analytics/gtm-script/GTMScript.utils'
import { PreConnect } from 'components/seo'
import { GTM_BLOCK_LIST, GTM_TRACKING_ID, SITE_URL } from 'configs'
import Document, { Head, Html, Main, NextScript } from 'next/document'
import Script from 'next/script'

class MyDocument extends Document {
  private gtmRestrictionConfig = getRestrictionConfig({ gtmBlocklist: GTM_BLOCK_LIST })

  render() {
    return (
      <Html>
        <Head>
          <PreConnect href={SITE_URL} />
          <GTMScript.PreConnect />

          {/* dataLayer - Global base code */}
          <Script
            id='datalayer-base'
            strategy='beforeInteractive'
            dangerouslySetInnerHTML={{
              /* Use a non-interaction event to avoid affecting bounce rate. */
              /* Use 'sendBeacon()' if the browser supports it. */
              __html: `
                  (function() {
                    window.dataLayer = window.dataLayer || [];
                    window.dataLayer.push({
                      originalLocation:
                        document.location.protocol +
                        '//' +
                        document.location.hostname +
                        document.location.pathname +
                        document.location.search
                    });
                    window.dataLayer.push({
                      non_interaction: true,
                      transport: 'beacon',
                    });
                    ${this.gtmRestrictionConfig}
                  })();
              `,
            }}
          />
        </Head>
        <body>
          <GTMScript.Body gtmTrackingId={GTM_TRACKING_ID} />
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
