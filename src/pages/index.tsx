import { Button } from 'components/button'
import { AlternatePaths } from 'components/localization'
import { useAlternatePaths } from 'hooks/localization/useAlternatePaths'
import type { NextPage, NextPageContext } from 'next'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import styles from 'styles/Home.module.css'

const Home: NextPage = () => {
  const { t } = useTranslation('common')
  const alternatePaths = useAlternatePaths()

  return (
    <>
      <AlternatePaths alternatePaths={alternatePaths} />

      <header>
        <h2>{t('brand')}</h2>
        <h3>{t('home:home')}</h3>
      </header>

      <div className={styles.container}>
        <Button />
      </div>
    </>
  )
}

export async function getStaticProps({ locale }: NextPageContext) {
  return {
    props: {
      ...(await serverSideTranslations(locale!, ['common', 'home'])),
    },
  }
}

export default Home
