import 'styles/globals.css'

import { GTMScript } from 'components/analytics/gtm-script'
import { GTM_TRACKING_ID } from 'configs'
import { useGTMRouteChange } from 'hooks/analytics/useGTMRouteChange'
import { appWithTranslation } from 'next-i18next'
import type { AppProps, NextWebVitalsMetric } from 'next/app'
import { gtmReportWebVitals } from 'utils/reports'

function MyApp({ Component, pageProps }: AppProps) {
  useGTMRouteChange()

  return (
    <>
      <GTMScript.Header gtmTrackingId={GTM_TRACKING_ID} />
      <Component {...pageProps} />
    </>
  )
}

export function reportWebVitals(metric: NextWebVitalsMetric) {
  gtmReportWebVitals(metric)
}

export default appWithTranslation(MyApp)
