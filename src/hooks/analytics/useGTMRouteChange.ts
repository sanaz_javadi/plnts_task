import { useRouter } from 'next/router'
import { useEffect } from 'react'
import { handleRouteChange } from 'utils/reports/gtm-route-change'

export const useGTMRouteChange = () => {
  const router = useRouter()

  useEffect(() => {
    router.events.on('routeChangeComplete', handleRouteChange)
    router.events.on('hashChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
      router.events.off('hashChangeComplete', handleRouteChange)
    }
  }, [router.events])
}
