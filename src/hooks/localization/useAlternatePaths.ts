import { SITE_URL } from 'configs'
import { useRouter } from 'next/router'

export interface IAlternatePath {
  href: string
  hrefLang: string
}

export type AlternatePaths = IAlternatePath[]

export const useAlternatePaths = (isValidPath = true): AlternatePaths | undefined => {
  const { asPath, locale: currentLocale, locales } = useRouter()

  return (
    (isValidPath || undefined) &&
    locales
      ?.filter((locale) => locale !== currentLocale)
      .map((locale) => ({ href: `${SITE_URL}/${locale}${asPath}`, hrefLang: locale }))
  )
}
