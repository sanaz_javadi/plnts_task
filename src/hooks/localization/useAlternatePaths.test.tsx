import { renderHook } from '@testing-library/react'

import { useAlternatePaths } from './useAlternatePaths'

let locale = 'fa'
jest.mock('next/router', () => ({
  useRouter() {
    return {
      asPath: '/',
      locale,
      locales: ['fa', 'en'],
    }
  },
}))

describe('useAlternatePaths', () => {
  it('useAlternatePaths works correctly', () => {
    const { result, rerender } = renderHook(() => useAlternatePaths())
    expect(result.current?.[0].hrefLang).toBe('en')

    locale = 'en'
    rerender()
    expect(result.current?.[0].hrefLang).toBe('fa')
  })
})
