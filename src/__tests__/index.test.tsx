import { render, screen } from '@testing-library/react'
import Home from 'pages/index'

describe('Home', () => {
  it('renders some buttons', () => {
    render(<Home />)

    const localButton = screen.queryByText('Local Button')

    expect(localButton).toBeInTheDocument()
  })
})
