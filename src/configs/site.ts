export const SITE_DOMAIN = process.env.NEXT_PUBLIC_DOMAIN || ''
export const SITE_URL = process.env.NEXT_PUBLIC_URL || ''
