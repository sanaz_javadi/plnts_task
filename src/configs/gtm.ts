export const GTM_TRACKING_ID = process.env.NEXT_PUBLIC_GTM_ID as string
export const GTM_BLOCK_LIST = ['customScripts']
