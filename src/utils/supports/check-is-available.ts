export const CheckIsAvailable = {
  window: () => typeof window !== 'undefined',
  gtag: () => window?.gtag instanceof Function,
  gtm: () => Array.isArray(window?.dataLayer),
}
