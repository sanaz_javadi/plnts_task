import { NextWebVitalsMetric } from 'next/app'
import { gtmEvent } from 'utils/analytics/gtm'

export function gtmReportWebVitals(metric: NextWebVitalsMetric) {
  const { id, name, label, value } = metric
  gtmEvent({
    eventName: label === 'web-vital' ? 'Web Vitals' : 'Next.js custom metric',
    'vital.name': name,
    'vital.value': Math.round(name === 'CLS' ? value * 1000 : value), // values must be integers
    'vital.id': id,
  })
}
