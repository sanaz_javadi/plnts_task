import { gtmPageView } from 'utils/analytics/gtm'

export const handleRouteChange = (url: URL) => {
  gtmPageView(url)
}
