export type GTagEvent = {
  eventName: string
  [key: string]: unknown
}

export const gtmPageView = (url: URL) => {
  gtmEvent({
    eventName: 'pageview',
    page: url,
  })
}

export const gtmEvent = ({ eventName, ...rest }: GTagEvent) => {
  window?.dataLayer?.push({
    // Set the event name
    event: eventName,
    ...rest,
  })
}
