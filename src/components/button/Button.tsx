import { IButtonProps } from './Button.model'

export const Button = ({ title = 'Local Button' }: IButtonProps) => {
  return <button>{title}</button>
}
