import Head from 'next/head'
import { memo } from 'react'

import { IAlternatePathsProps } from './AlternatePaths.model'

export const AlternatePaths = memo(function AlternatePaths({
  alternatePaths,
}: IAlternatePathsProps) {
  return (
    <Head>
      {alternatePaths?.map(({ href, hrefLang }) => (
        <link key={href} rel='alternate' hrefLang={hrefLang} href={href} />
      ))}
    </Head>
  )
})
