import { AlternatePaths } from 'hooks/localization/useAlternatePaths'

export interface IAlternatePathsProps {
  alternatePaths?: AlternatePaths
}
