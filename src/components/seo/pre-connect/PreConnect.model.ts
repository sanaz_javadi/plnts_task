export interface IPreConnectProps {
  href: string
  crossOrigin?: CrossOrigin
}
