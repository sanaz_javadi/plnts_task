import { IPreConnectProps } from './PreConnect.model'

export const PreConnect = ({ href, crossOrigin }: IPreConnectProps) => {
  return <link rel='preconnect' href={href} {...(crossOrigin ? { crossOrigin } : null)} />
}
