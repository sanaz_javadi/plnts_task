// TODO: write rest of BottomNavigation tests
import { render, screen } from '@testing-library/react'

import { BottomNavigation } from './BottomNavigation'

describe('BottomNavigation', () => {
  it('renders the BottomNavigation', () => {
    const title = 'title'
    render(<BottomNavigation title='title' />)

    const element = screen.getByText(title)

    expect(element).toBeInTheDocument()
  })
})
