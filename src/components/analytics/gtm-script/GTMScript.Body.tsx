import { ITrackingID } from './GTMScript.model'

export const GTMScriptBody = ({ gtmTrackingId }: ITrackingID) => {
  return (
    <noscript>
      <iframe
        src={`https://www.googletagmanager.com/ns.html?id=${gtmTrackingId}`}
        height='0'
        width='0'
        style={{ display: 'none', visibility: 'hidden' }}
      />
    </noscript>
  )
}
