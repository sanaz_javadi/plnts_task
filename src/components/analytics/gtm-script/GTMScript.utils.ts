import { IGTMRestrictionConfig } from './GTMScript.model'

export const getRestrictionConfig = ({
  gtmAllowlist,
  gtmBlocklist,
}: IGTMRestrictionConfig) => {
  if (!gtmAllowlist && !gtmBlocklist) return ''
  const restrictionList = {
    ...(gtmAllowlist && { 'gtm.allowlist': gtmAllowlist }),
    ...(gtmBlocklist && { 'gtm.blocklist': gtmBlocklist }),
  }
  return restrictionList
    ? `window.dataLayer.push(${JSON.stringify(restrictionList)});`
    : ''
}
