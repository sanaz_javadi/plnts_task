import { PreConnect } from 'components/seo'

export const GTMScripPreConnect = () => {
  return <PreConnect href='https://www.google-analytics.com' crossOrigin='anonymous' />
}
