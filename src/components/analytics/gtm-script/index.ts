import { GTMScriptBody } from './GTMScript.Body'
import { GTMScriptHeader } from './GTMScript.Header'
import { GTMScripPreConnect } from './GTMScript.PreConnect'

export * from './GTMScript.model'

export const GTMScript = {
  PreConnect: GTMScripPreConnect,
  Header: GTMScriptHeader,
  Body: GTMScriptBody,
}
