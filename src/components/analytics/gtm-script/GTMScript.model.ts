export interface ITrackingID {
  gtmTrackingId: string
}

export interface IGTMRestrictionConfig {
  gtmAllowlist?: string[]
  gtmBlocklist?: string[]
}
