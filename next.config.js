/** @type {import('next').NextConfig} */

const withPlugins = require('next-compose-plugins')
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE_BUNDLE === 'true',
})
const { i18n } = require('./next-i18next.config')
const { securityHeaders } = require('./rootConfigs/next/security-headers')

const nextConfig = {
  output: 'standalone',
  reactStrictMode: true,
  trailingSlash: false,
  i18n,
  async headers() {
    return [
      {
        source: '/:path*', // Apply these headers to all routes in the application.
        headers: securityHeaders,
      },
    ]
  },
}

module.exports = withPlugins([[withBundleAnalyzer]], nextConfig)
