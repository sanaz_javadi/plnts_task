# [0.2.0](https://git.snapp.ninja/frontend/customer/compare/v0.1.0...v0.2.0) (2022-07-25)

### Bug Fixes

- **eslint:** fixes typescript checking ([c07b443](https://git.snapp.ninja/frontend/customer/commit/c07b4430cc820387c993c63ca714e656aff5c1c0))
- **jest-config:** excludes vscode path ([9b2e6bd](https://git.snapp.ninja/frontend/customer/commit/9b2e6bd477d5f60a3c6a86e7133889c141080398))
- **ts:** fix buildinfo file in lint-staged ([948dbdd](https://git.snapp.ninja/frontend/customer/commit/948dbddce2cee23af23e008d36fffaea5743aaf6))
- **vscode:** some vscode changes ([2caa1ff](https://git.snapp.ninja/frontend/customer/commit/2caa1ff146e548f0f19f1fac267f460416bb6828))

### Features

- **bundle:** add bundle analyzer ([a5b2c67](https://git.snapp.ninja/frontend/customer/commit/a5b2c67b9bbfbe489168e48e1592f709a7393248))
- **gtm:** adds gtm & remove gtag ([0c334c2](https://git.snapp.ninja/frontend/customer/commit/0c334c24a7369f8eeda3aa5f188699283f43003e))
- **i18:** implements next-i18next ([04e4ab0](https://git.snapp.ninja/frontend/customer/commit/04e4ab0fae3416f2dd1d5590ab31f5e85743b977))
- **scaffolders:** adds component scaffolder ([23387cd](https://git.snapp.ninja/frontend/customer/commit/23387cd6cb4316f54f2355e86366d8a56a1a78d7))

# [0.1.0](https://github.com/amir04lm26/snappshop-customer/compare/v0.0.0...v0.1.0) (2022-07-19)

### Bug Fixes

- **lint-staged:** fixes lint-staged & exports the config ([6a13c92](https://github.com/amir04lm26/snappshop-customer/commit/6a13c92487d387834057f093e42257832d06be8a))
- **yarn hooks:** preinstall hook ([6ba9634](https://github.com/amir04lm26/snappshop-customer/commit/6ba963410eb29f2d50188df95d308509e63da4ce))

### Features

- **ga & reportWebVitals:** implements ga & reportWebVitals ([6aeff58](https://github.com/amir04lm26/snappshop-customer/commit/6aeff585b305284687b5cd359365ec97ce5707bd))
- **release:** change release package ([0fda1b7](https://github.com/amir04lm26/snappshop-customer/commit/0fda1b710ec7befa110763e48b5427d1c13471a4))
- **robot.txt:** adds robot.txt ([37647e0](https://github.com/amir04lm26/snappshop-customer/commit/37647e0d429ea174f74b999cb08abf69d45373b8))
- **vscode:** adds some vscode configs ([1b17d37](https://github.com/amir04lm26/snappshop-customer/commit/1b17d3732ae4248c559b38c711dc096f2429b52f))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.1](https://github.com/mokkapps/changelog-generator-demo/compare/v0.0.0...v0.0.1) (2022-07-17)

### Features

- **ga & reportWebVitals:** implements ga & reportWebVitals ([6aeff58](https://github.com/mokkapps/changelog-generator-demo/commits/6aeff585b305284687b5cd359365ec97ce5707bd))
- **robot.txt:** adds robot.txt ([37647e0](https://github.com/mokkapps/changelog-generator-demo/commits/37647e0d429ea174f74b999cb08abf69d45373b8))
- **vscode:** adds some vscode configs ([1b17d37](https://github.com/mokkapps/changelog-generator-demo/commits/1b17d3732ae4248c559b38c711dc096f2429b52f))

### Bug Fixes

- **lint-staged:** fixes lint-staged & exports the config ([6a13c92](https://github.com/mokkapps/changelog-generator-demo/commits/6a13c92487d387834057f093e42257832d06be8a))
- **yarn hooks:** preinstall hook ([6ba9634](https://github.com/mokkapps/changelog-generator-demo/commits/6ba963410eb29f2d50188df95d308509e63da4ce))

### Other

- **npm:** disables npm ([c92e483](https://github.com/mokkapps/changelog-generator-demo/commits/c92e4839a9e82c6ef1e632b77753a9b0790364f8))
- **yarn:** adds bundle-phobia check to install event ([1570e72](https://github.com/mokkapps/changelog-generator-demo/commits/1570e72edd4ad4392374e96aa78fab40eccd4943))

## 0.0.0 (2022-07-16)

### Features

- **lint-staged:** adds lint-staged & adds some cypress scripts ([43c3c11](https://github.com/mokkapps/changelog-generator-demo/commits/43c3c11656987c5a37bec2cd632ade59bebb69ae))
- **project:** initialize the project ([36a2361](https://github.com/mokkapps/changelog-generator-demo/commits/36a2361a9fc3763997e59a3b7521514e46f2e36f))
- **standard-version:** adds standard-version ([480441c](https://github.com/mokkapps/changelog-generator-demo/commits/480441c6c499befa1237f1b43a393c8a99735898))
