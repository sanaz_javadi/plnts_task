const path = require('path')

module.exports = {
  i18n: {
    locales: ['fa', 'en'],
    defaultLocale: 'fa',
    localeDetection: true,
    localePath: path.resolve('./public/locales'),
  },
}
